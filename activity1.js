
// Number 1
db.users.insertMany([
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"email": "jfirrelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"email": "pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email": "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
	]);

db.users.find()

// Number 2
db.courses.insertMany([
		{
			"name": "Professional Development",
			"price": 10000
		},
		{
			"name": "Business Processing",
			"price": 13000
		},
	]);

db.courses.find()

// Number 3
db.users.find({"isAdmin": false})

//Number 4
db.courses.updateMany(
	{},
	{
		$set: {
			"enrollees": [
					{
						"userId": ObjectId("620cc566d5e58a85f4d1a882")
					},
					{
						"userId": ObjectId("620cc566d5e58a85f4d1a883")
					},
					{
						"userId": ObjectId("620cc566d5e58a85f4d1a884")
					}
				]
		}
	}
);