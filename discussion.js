db.inventory.insertMany([
	{
		"name": "Javascript for Beginners",
		"author": "James Doe",
		"price": 5000,
		"stocks": 50,
		"publisher": "JS Publishing House"
	},
	{
		"name": "HTML and CSS",
		"author": "John Tomas",
		"price": 2500,
		"stocks": 38,
		"publisher": "NY Publisher"
	},
	{
		"name": "Web Development Fundamentals",
		"author": "Noah Jimenez",
		"price": 3000,
		"stocks": 10,
		"publisher": "Big Idea Publishing House"
	},
	{
		"name": "Java Programming",
		"author": "David Michael",
		"price": 10000,
		"stocks": 100,
		"publisher": "JS Publishing House"
	}			
])

//Comparison Query Operators
//$gt/$gte operator
/*
	Syntax: db.collectionName.find({ field: {$gt: value} })
		    db.collectionName.find({ field: {$gte: value} })

*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});

//$lt/$lte operator

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});

//$ne operator
db.inventory.find({
	"stocks": {
		$ne: 50
	}
});

//$eq operator
db.inventory.find({
	"stocks": {
		$eq: 50
	}
});

db.inventory.find({
	"stocks": 50
});

//$in operator
/*
	Syntax: db.collectionName.find({ field: { $in: [values]} })
*/
db.inventory.find({
	"price": {
		$in: [10000, 5000]
	}
});

//Logical Query Operator
//$and operator
/*
	db.collectionName.find({
		$and: [
			{ fieldA: valueA},
			{ fieldB: valueB}
		]
	})
*/

db.inventoryfind({
	$and: [
		{ 
			"stocks": { $ne:50 }
		},
		{
			"price": { $ne: 5000}
		}

	]
});

db.inventory.find({
	"stocks": {
		$ne: 50
	},
	"price": {
		$ne: 5000
	}
});

//$or operator
db.inventory.find({
	$or: [
		{
			"name": "HTML and CSS"
		},
		{
			"publisher": "JS Publishing House"
		}
	]
});

db.inventory.find({
	$or: [
		{
			"author": "James Doe"
		},
		{
			"price": {
				$lte: 5000
			}
		}
	]
});

//Field Projection
/*
	Syntax: db.collectionName.find({criteria}, {field: 1})
*/

//Inclusion
db.inventory.find(
	{
		"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1
	}
)

//Exclusion
db.inventory.find(
	{
		"author": "Noah Jimenez"
	},
	{
		"price": 0,
		"stocks": 0
	}

)

db.inventory.find(
	{
		"price": {$lte: 5000}
	},
	{
		"_id": 0,
		"name": 1,
		"author": 1
	}

);

//Evaluation Query Operator
//#regex operator
/*
	Syntax: db.collectionName.find({field: {$regex: 'pattern', $options: 'optionsValue'}})
*/

//Case sensitive Query
db.inventory.find({
	"author": {
		$regex: 'M'
	}
});

//Case insensitive Query
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
});