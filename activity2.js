db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		},

		{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations"
		},

		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"email": "janedoe@mail.com",
			"department": "HR"
		}
	]);

db.users.find();

// With s or d

db.users.find({
	$or: [
		{
			"firstName": {
				$regex: 'S',
				$options: '$i'
			}
		},
		{
			"lastName": {
				$regex: 'D',
				$options: '$i'
			}
		}

	]
},
{
	"_id": 0,
	"firstName": 1,
	"lastName": 1
});

// HR and age >= 70

db.users.find({
	$and: [
		{
			"department": "HR"
		},
		{
			"age": {
				$gte: 70
			}
		}
	]
});

// e in firstName and age <= 30
db.users.find({
	$and: [
		{
			"firstName": {
				$regex: 'E',
				$options: '$i'
			}
		},
		{
			"age": {
				$lte: 30
			}
		}
	]
});